package epam.com;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/delivery/*")
public class MyFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {

    Optional<String> newPizzaOrder = Optional.ofNullable(servletRequest.getParameter("pizza_name"));
    if (newPizzaOrder.filter(user -> user.equalsIgnoreCase("Vegie")).isPresent()) {
      ((HttpServletResponse) servletResponse).sendError(403, "Vegie pizza is forbidden");
      return;
    }
    filterChain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {

  }
}
