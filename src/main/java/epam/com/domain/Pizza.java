package epam.com.domain;

public class Pizza {
  private static int unique = 1;
  private int id;
  private String pizzaName;
  private String deliveryType;

  public Pizza(String pizzaName, String deliveryType) {
    this.pizzaName = pizzaName;
    this.deliveryType = deliveryType;
    id = unique;
    unique++;
    if(unique == 100) {
      unique = 1;
    }
  }

  public int getId() {
    return id;
  }

  public String getDeliveryType() {
    return deliveryType;
  }

  public String getPizzaName() {
    return pizzaName;
  }

  @Override
  public String toString() {
    return "id = " + id + " pizza = " + pizzaName + " delivery type = " + deliveryType;
  }
}
