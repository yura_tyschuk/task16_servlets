package epam.com;

import epam.com.domain.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/delivery/*")
public class UserServlet extends HttpServlet {

  private static Map<Integer, Pizza> pizzaOrders = new HashMap<>();

  @Override
  public void init() throws ServletException {
    Pizza pizaOrder1 = new Pizza("Peperoni", "Normal delivery");
    pizzaOrders.put(pizaOrder1.getId(), pizaOrder1);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><head><body>");
    out.println("<p> <a href='delivery'>REFRESH</a> </p>");
    out.println("<h2>Pizza list </h2>");
    for (Pizza pizza : pizzaOrders.values()) {
      out.println("<p>" + pizza + "</p>");
    }

    out.println("<form action='delivery' method='POST'>\n"
        + " User name: <input type='text' required name='pizza_name'>\n"
        + "<select name='delivery_type'> <option> Fast delivery </option> \n"
        + "<option>Normal delivery</option> </select>\n"
        + "<p><input type='submit' value='Select'></p>\n"
        + "</form>");

    out.println("<form>\n"
        + " <p><b>Delete element</b></p>\n"
        + " <p> Order id: <input type='text' name='pizzaOrder_id'>\n"
        + " <input type='button' onclick='remove(this.form.pizzaOrder_id.value)' name='ok' value = 'Delete element'\n"
        + " </p>\n"
        + " </form>\n");

    out.println("<script type='text/javascript'>\n"
        + " function remove(id) { fetch('delivery/' + id, {method: 'DELETE'}); } \n"
        + "</script>");

    out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
    out.println("<p>Method: " + req.getMethod() + "</p>");
    out.println("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String newUserName = req.getParameter("pizza_name");
    String deliveryType = req.getParameter("delivery_type");
    Pizza newPizza = new Pizza(newUserName, deliveryType);
    pizzaOrders.put(newPizza.getId(), newPizza);
    doGet(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    String id = req.getRequestURI();
    id = id.replace("/task16_servlets-1.0-SNAPSHOT/delivery/", "");
    pizzaOrders.remove(Integer.parseInt(id));
  }

  @Override
  public void destroy() {

  }


}
